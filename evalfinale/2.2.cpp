#include <iostream>
#include <string>

class Personne {
public:

    Personne(const std::string& nom) : nom_(nom) {}

   
    const std::string& getNom() const {
        return nom_;
    }

private:
    std::string nom_;
};

void AffichePersonne(const Personne& personne) {
   
    std::cout << "Personne(" << &personne << "): " << personne.getNom() << std::endl;
}

int main() {
    
    Personne p("xxxx");

    AffichePersonne(p);

    return 0;
}
#include <iostream>
#include <string>

class Personne {
public:
    
    Personne(const std::string& nom) : nom_(nom) {}

    
    const std::string& getNom() const {
        return nom_;
    }

private:
    std::string nom_;
};

void AffichePersonne(const Personne& personne) {
   
    std::cout << "Personne(" << &personne << "): " << personne.getNom() << std::endl;
}

Personne* CreerPersonne() {
    std::string nom;
    std::cout << "Entrez le nom de la personne: ";
    std::getline(std::cin, nom);
    Personne* personne = new Personne(nom);
    return personne;

}
 
int main() {

        Personne* p = CreerPersonne();


        AffichePersonne(*p);


        delete p;

        return 0;
    }
